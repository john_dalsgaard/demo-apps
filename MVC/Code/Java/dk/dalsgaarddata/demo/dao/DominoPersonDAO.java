/**
 * Concrete implementation of a PersonDAO with the use of a Domino database
 */
package dk.dalsgaarddata.demo.dao;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openntf.domino.Document;
import org.openntf.domino.View;
import org.openntf.domino.ViewEntry;

import com.ibm.commons.util.StringUtil;

import dk.dalsgaarddata.demo.base.BaseDominoDAO;
import dk.dalsgaarddata.demo.data.Person;

/**
 * @author jda, 2013.11.13
 * 
 */
public class DominoPersonDAO extends BaseDominoDAO implements PersonDAO {

	private static final String VIEW_NAME = "Persons";
	private static final String FORM_NAME = "Person";

	private View getView() {
		return super.getView(VIEW_NAME);
	}

	private Person loadFromEntry(ViewEntry entry) {
		// Create a new object and load it with data
		Person person = new Person();
		person.setKey((String) entry.getColumnValues().get(0));
		person.setName((String) entry.getColumnValues().get(1));
		person.setAddress((String) entry.getColumnValues().get(2));
		person.setZip((String) entry.getColumnValues().get(3));
		person.setCity((String) entry.getColumnValues().get(4));
		person.setEmail((String) entry.getColumnValues().get(5));
		person.setYearBorn(toInteger(entry.getColumnValues().get(6)));
		person.setUnid(entry.getUniversalID());
		return person;
	}

	private Person loadFromDocument(Document doc) {
		// Create a new object and load it with data
		Person person = new Person();
		person.setKey(doc.getItemValueString("Key"));
		person.setName(doc.getItemValueString("Name"));
		person.setAddress(doc.getItemValueString("Address"));
		person.setZip(doc.getItemValueString("Zip"));
		person.setCity(doc.getItemValueString("City"));
		person.setEmail(doc.getItemValueString("Email"));
		person.setYearBorn(toInteger(doc.getItemValueString("YearBorn")));
		person.setUnid(doc.getUniversalID());
		return person;
	}

	public Map<String, Person> getAllPersons() {
		if (isViewDirty()) {
			getView().refresh();
		}
		Map<String, Person> all = new ConcurrentHashMap<String, Person>();
		for (ViewEntry entry : getView().getAllEntries()) {
			Person person = loadFromEntry(entry);
			all.put(person.getKey(), person);
		}
		return all;
	}

	public Person loadPerson(String key) {
		Person person = null;
		if (!StringUtil.isEmpty(key)) {
			if (isViewDirty()) {
				getView().refresh();
			}
			if (isUnid(key)) {
				Document doc = getDb().getDocumentByUNID(key);
				if (null != doc) {
					person = loadFromDocument(doc);
				}
			} else {
				ViewEntry entry = getView().getEntryByKey(key);
				if (null != entry) {
					person = loadFromEntry(entry);
				}
			}
		}
		return person;
	}

	public void savePerson(Person person) {
		Document doc = null;
		if (!StringUtil.isEmpty(person.getKey())) {
			doc = getView().getDocumentByKey(person.getKey());
		}
		if (null == doc) {
			debug("savePerson: Creating new person");
			doc = getDb().createDocument();
			person.setUnid(doc.getUniversalID());
			updateField(doc, "Unid", person.getUnid());
			if (StringUtil.isEmpty(person.getKey())) {
				person.setKey(person.getUnid());
			}
		} else {
			debug("savePerson: Updating person. Key=" + person.getKey());
		}

		updateField(doc, "Key", person.getKey());
		updateField(doc, "Name", person.getName());
		updateField(doc, "Address", person.getAddress());
		updateField(doc, "Zip", person.getZip());
		updateField(doc, "City", person.getCity());
		updateField(doc, "Email", person.getEmail());
		updateField(doc, "YearBorn", person.getYearBorn());
		updateField(doc, "Form", FORM_NAME);
		if (doc.isDirty()) {
			doc.save();
			setViewDirty(true);
		}
	}

	public void removePerson(Person person) {
		if (null == person) return;
		if (!StringUtil.isEmpty(person.getKey())) {
			Document doc = getView().getDocumentByKey(person.getKey());
			if (null != doc) {
				doc.remove(true);
				person = null;
				setViewDirty(true);
			}
		}
	}

	public void removePerson(String key) {
		removePerson(loadPerson(key));
	}

}
