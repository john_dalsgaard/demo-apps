// Solves a problem where the partial refresh removes the focus from the field...
function FocusHandler() {
	this.currentField = "";
};

// Static methods (call directly on class as: FocusHandler.reset()...
FocusHandler.debug = function(m) {
	// Uncomment to show log messages...
//	console.log(m);
}
FocusHandler.set = function(fld) {
	this.currentField = fld;
	FocusHandler.debug("Capture focus field: " + this.currentField);
}
FocusHandler.reset = function() {
	FocusHandler.debug("Reset focus field");
	this.set("");
}
FocusHandler.focus = function() {
	if(this.currentField != ""){
		try {
			FocusHandler.debug("Set focus: " + this.currentField);
			dojo.byId(this.currentField).focus();
		} catch(e) {}
	}
}
FocusHandler.focusNext = function() {
	if(this.currentField != ""){
		try {
			var nextField = this.currentField;
			var prevComp = this.currentField.split(':');
			if(prevComp.length > 2) {
				var rowNo = parseInt(prevComp[prevComp.length-2]);
				rowNo++;
				prevComp[prevComp.length-2] = rowNo+'';
				nextField = prevComp.join(':');
			}
			FocusHandler.debug("Set focus: " + nextField);
			dojo.byId(nextField).focus();
		} catch(e) {
			try {
				dojo.byId(this.currentField).focus();
			} catch(e) {
			}
		}
	}
}

// Set focus on load (fishingTripEntry)
var setInitialFocus = function(fields) {
	try {
		var firstField = fields;
		for(var i=0; i<firstField.length; i++) {
			var fld = firstField[i];
			if(dojo.byId(fld)) {
				FocusHandler.debug("Check id: " + fld);
				dojo.byId(fld).focus();
			}
		}
	} catch (e) {}
};
