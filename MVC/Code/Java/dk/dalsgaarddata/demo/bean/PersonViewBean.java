package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.util.List;

import com.ibm.commons.util.StringUtil;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.data.Car;
import dk.dalsgaarddata.demo.data.Person;

/**
 * @author jda, 2013.11.13
 * 
 */
public class PersonViewBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Person person = null;
	private List<Car> cars = null;

	public PersonViewBean() {
	}

	public void loadPerson(String key) {
		setPerson(DataBean.getCurrentInstance().getPersonFacade().findPerson(key));
		if (null != person) {
			debug("Person loaded: " + person.getName());
		}
	}

	public void editPerson() {
		Util.redirectToXpage("editPerson.xsp?id=" + getPerson().getKey());
	}

	// Getters & Setters
	public void setPerson(Person person) {
		this.person = person;
	}

	public Person getPerson() {
		if (null == person) {
			String id = Util.getUrlParam("id");
			if (StringUtil.isEmpty(id)) {
				debug("getPerson. Create new empty Person");
				setPerson(new Person()); // To allow reference to object...
			} else {
				// This is a page opened with a key to an existing fishing trip
				debug("Open existing Person. id=" + id);
				loadPerson(id);
			}
		}
		return person;
	}

	public void cancel() {
		Util.redirectToXpage("persons.xsp");
	}

	public List<Car> getCars() {
		if (null == cars) {
			cars = DataBean.getCurrentInstance().getPersonFacade().getPersonsCars(getPerson().getKey());
		}
		return cars;
	}

	public int getCarCount() {
		return getCars().size();
	}

}
