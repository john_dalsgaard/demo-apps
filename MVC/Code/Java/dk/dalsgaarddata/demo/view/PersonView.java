package dk.dalsgaarddata.demo.view;

import java.util.List;

import dk.dalsgaarddata.demo.base.BaseData;
import dk.dalsgaarddata.demo.data.Car;
import dk.dalsgaarddata.demo.data.Person;

/**
 * @author jda, 2013.11.25
 * 
 */
public class PersonView extends BaseData {
	private static final long serialVersionUID = 1L;
	private Person person = null;
	private List<Car> cars = null;

	public PersonView(Person person) {
		this.person = person;
	}

	public Person getPerson() {
		return person;
	}

	public List<Car> getCars() {
		return cars;
	}

	public int getCarCount() {
		if (null == getCars()) {
			return 0;
		}
		return getCars().size();
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
}