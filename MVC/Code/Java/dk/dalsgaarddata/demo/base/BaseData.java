package dk.dalsgaarddata.demo.base;

import java.io.Serializable;

/**
 * @author jda, 2013.11.13
 * 
 */
public class BaseData implements Serializable {
	private static final long serialVersionUID = 1L;
	private String key = null;
	private String unid = null;

	public String getKey() {
		return key;
	}

	public String getUnid() {
		return unid;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		BaseData other = (BaseData) obj;
		if (key == null) {
			if (other.key != null) return false;
		} else if (!key.equals(other.key)) return false;
		return true;
	}

}