package dk.dalsgaarddata.demo.data;

import dk.dalsgaarddata.demo.base.BaseData;

/**
 * @author jda, 2013.11.13
 * 
 */
public class Person extends BaseData {
	private static final long serialVersionUID = 1L;
	private String name = null;
	private String address = null;
	private String zip = null;
	private String city = null;
	private String email = null;
	private Integer yearBorn = null;

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getZip() {
		return zip;
	}

	public String getCity() {
		return city;
	}

	public String getEmail() {
		return email;
	}

	public Integer getYearBorn() {
		return yearBorn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setYearBorn(Integer yearBorn) {
		this.yearBorn = yearBorn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((yearBorn == null) ? 0 : yearBorn.hashCode());
		result = prime * result + ((zip == null) ? 0 : zip.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		Person other = (Person) obj;
		if (address == null) {
			if (other.address != null) return false;
		} else if (!address.equals(other.address)) return false;
		if (city == null) {
			if (other.city != null) return false;
		} else if (!city.equals(other.city)) return false;
		if (email == null) {
			if (other.email != null) return false;
		} else if (!email.equals(other.email)) return false;
		if (name == null) {
			if (other.name != null) return false;
		} else if (!name.equals(other.name)) return false;
		if (yearBorn == null) {
			if (other.yearBorn != null) return false;
		} else if (!yearBorn.equals(other.yearBorn)) return false;
		if (zip == null) {
			if (other.zip != null) return false;
		} else if (!zip.equals(other.zip)) return false;
		return true;
	}

}
