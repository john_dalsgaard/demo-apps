package dk.dalsgaarddata.demo.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.validator.ValidatorException;
import javax.xml.bind.ValidationException;

/**
 * @author jda, 2013.11.13
 * 
 */
public class BaseCRUDFacade extends Base implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * Base class for all CRUD facade implementations.
	 */
	private Map<String, String> errMessages = null;
	private Map<String, String> convErrorMessages = null;

	protected boolean runAsSigner = false;

	public void setRunAsSigner(boolean b) {
		runAsSigner = b;
	}

	protected void initErrors() {
		errMessages = new HashMap<String, String>();
	}

	protected void addErrorMessage(String id, String m) {
		if (null == errMessages) {
			initErrors();
		}
		errMessages.put(id, m);
	}

	protected boolean isError() {
		return (errMessages.size() > 0);
	}

	public Map<String, String> getErrors() {
		return errMessages;
	}

	protected void initConversionErrors() {
		convErrorMessages = new HashMap<String, String>();
	}

	public void addConversionErrorMessage(String id, String m) {
		if (null == convErrorMessages) {
			initConversionErrors();
		}
		convErrorMessages.put(id, m);
	}

	protected boolean isConversionError() {
		return (null == convErrorMessages) ? false : (convErrorMessages.size() > 0);
	}

	public Map<String, String> getConversionErrors() {
		return convErrorMessages;
	}

	public void throwValidationError(String msg) throws ValidatorException {
		throw new ValidatorException(new FacesMessage(msg));
	}

	protected void throwValidationErrors() throws ValidationException {
		if (isError()) {
			StringBuilder msg = new StringBuilder();
			for (String m : errMessages.values()) {
				if (msg.length() > 0) {
					msg.append("<br />");
				}
				msg.append(m);
			}
			throw new ValidationException(msg.toString());
		}
	}

}
