package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.cache.CacheContainer;
import dk.dalsgaarddata.demo.dao.facade.CarCRUDFacade;
import dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade;
import dk.dalsgaarddata.demo.data.Person;
import dk.dalsgaarddata.demo.view.PersonView;

/**
 * @author jda, 2013.11.13
 * 
 */
public class DataBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;

	//	private CarCRUDFacade carFacade = null;
	//	private PersonCRUDFacade personFacade = null;

	public static DataBean getCurrentInstance() {
		// This is a neat way to get a handle on the instance of this bean in the session scope from other Java code...
		FacesContext context = Util.getFacesContext();
		return (DataBean) context.getApplication().getVariableResolver().resolveVariable(context, "Data");
	}

	public CarCRUDFacade getCarFacade() {
		//		if (null == carFacade) {
		//			debug("Create carFacade...");
		//			carFacade = new CarCRUDFacade();
		//		}
		//		return carFacade;
		return CacheContainer.INSTANCE.getCarFacade();
	}

	public boolean isPersonFacadeInitialized() {
		return CacheContainer.INSTANCE.isPersonFacadeInitialized();
	}

	public PersonCRUDFacade getPersonFacade() {
		//		if (null == personFacade) {
		//			debug("Create personFacade...");
		//			personFacade = new PersonCRUDFacade();
		//		}
		//		return personFacade;
		return CacheContainer.INSTANCE.getPersonFacade();
	}

	public List<PersonView> getPersons() {
		List<PersonView> persons = new ArrayList<PersonView>();
		for (Person person : getPersonFacade().getAllPersons()) {
			PersonView view = new PersonView(person);
			view.setCars(getPersonFacade().getPersonsCars(person.getKey()));
			persons.add(view);
		}

		return persons;
	}
}
