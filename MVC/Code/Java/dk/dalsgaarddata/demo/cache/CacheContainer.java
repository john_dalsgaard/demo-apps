package dk.dalsgaarddata.demo.cache;

import dk.dalsgaarddata.demo.bean.MetaDataBean;
import dk.dalsgaarddata.demo.dao.facade.CarCRUDFacade;
import dk.dalsgaarddata.demo.dao.facade.PersonCRUDFacade;

public enum CacheContainer {
	INSTANCE;
	//	private MetaDataBean meta = null;
	//	private CarCRUDFacade carFacade = null;
	//	private PersonCRUDFacade personFacade = null;

	private MetaDataBean meta = new MetaDataBean();
	private CarCRUDFacade carFacade = new CarCRUDFacade();
	private PersonCRUDFacade personFacade = new PersonCRUDFacade();

	//	private final MetaDataBean meta = new MetaDataBean();
	//	private final CarCRUDFacade carFacade = new CarCRUDFacade();
	//	private final PersonCRUDFacade personFacade = new PersonCRUDFacade();

	private CacheContainer() {
		System.out.println("Instantiating enum CacheContainer...");
		//		carFacade = new CarCRUDFacade();
		//		personFacade = new PersonCRUDFacade();
		//		meta = new MetaDataBean();
	}

	public PersonCRUDFacade getPersonFacade() {
		if (null == personFacade) {
			System.out.println("Create personFacade...");
			personFacade = new PersonCRUDFacade();
		}
		return personFacade;
	}

	public boolean isPersonFacadeInitialized() {
		return null != personFacade;
	}

	public CarCRUDFacade getCarFacade() {
		if (null == carFacade) {
			System.out.println("Create carFacade...");
			carFacade = new CarCRUDFacade();
		}
		return carFacade;
	}

	public MetaDataBean getMeta() {
		if (null == meta) {
			System.out.println("Create meta...");
			meta = new MetaDataBean();
		}
		return meta;
	}

}
