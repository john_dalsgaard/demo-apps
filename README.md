# README #

This is a simple demo database that will show you some features of using Expression Language, Java, validators and component binding in a practice. The demo database is not "complete" as such - nor is it at the moment consistent in the way entry fields are validated. Some use Dojo features, some use "traditional" settings in validators - and some use component binding and a Java validator class (these last features have been added to the most recent version to demonstrate this concept).

### Purpose of this app. ###

* Show ways of doing validation in XPages
* Show examples using component binding inspired by Tim Tripcony [showing the concept in a video](http://www.youtube.com/watch?v=WPid1_HfvFM) #codefortim
* The original database was in support of my [presentation at DanNotes November 2013](http://www.slideshare.net/JohnDalsgaard/mvc-and-ibm-xpages-from-dannotes-in-korsr-dk-28-november-2013). However, the database has now moved "ahead" of the presentation. The most important improvement being to allow the validation to be called from the "Model" layer using View scoped beans (and NOT Session scoped beans as originally described). This is a major change since it simplifies a lot of the data handling...
* Please note that this app. will be updated now and then to demonstrate new features - or better ways of doing what is already shown.

### How do I get set up? ###

* Download the project (it is an on-disk-project of the NSF)
* Create a new NSF from the repository - and you are ready to explore and play with the code
* If you want to try the preload (which is breaking at the moment) - do the following:
 * - Add: XspPreload=1 to notes.ini
 * - Add: XspPreloadDB=mvc.nsf/preload.xsp to notes.ini
 * - On the server console: restart task http

### Who do I talk to? ###

* Any questions? You may contact me on: john@dalsgaard-data.dk
* You can also see more about what I and we do - and follow my blog on our [homepage](http://www.dalsgaard-data.eu/)