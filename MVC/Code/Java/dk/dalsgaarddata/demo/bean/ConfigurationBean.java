package dk.dalsgaarddata.demo.bean;

import java.io.Serializable;

import javax.faces.context.FacesContext;

import dk.dalsgaarddata.demo.base.BaseBean;
import dk.dalsgaarddata.demo.base.Util;
import dk.dalsgaarddata.demo.cache.CacheContainer;

/**
 * @author jda, 2013.11.13
 * 
 */
public class ConfigurationBean extends BaseBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static boolean debug = true;
	public static String SYSTEM_DATE_FORMAT = "dd-MM-yyyy";
	public static String SYSTEM_DATETIME_FORMAT = "dd-MM-yyyy hh:mm";

	//private final MetaDataBean meta;

	public ConfigurationBean() {
		//meta = new MetaDataBean();
	}

	public static ConfigurationBean getCurrentInstance() {
		// This is a neat way to get a handle on the instance of this bean in the application scope from other Java code...
		FacesContext context = Util.getFacesContext();
		ConfigurationBean bean = (ConfigurationBean) context.getApplication().getVariableResolver().resolveVariable(context, "Configuration");
		return bean;
	}

	public static boolean isDebug() {
		return debug;
	}

	public boolean isDebugEnabled() {
		return debug;
	}

	public MetaDataBean getMeta() {
		//return meta;
		return CacheContainer.INSTANCE.getMeta();
	}

	public String getSiteRootUrl() {
		return Util.getSiteRootUrl();
	}

}
