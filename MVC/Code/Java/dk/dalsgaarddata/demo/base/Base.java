package dk.dalsgaarddata.demo.base;

import dk.dalsgaarddata.demo.bean.ConfigurationBean;

/**
 * @author jda, 2013.11.13
 * 
 */
public class Base {
	protected void debug(String m) {
		if (ConfigurationBean.isDebug()) {
			debug(m, true);
		}
	}

	protected void debug(String m, boolean isDebug) {
		if (isDebug) System.out.println(this.getClass().getSimpleName() + ": " + m);
	}

	protected void error(String m) {
		System.err.println(this.getClass().getSimpleName() + " ERROR - " + m);
	}

}
